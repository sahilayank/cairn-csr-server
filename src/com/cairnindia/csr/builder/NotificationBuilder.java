package com.cairnindia.csr.builder;

import java.util.Collections;

import com.cairnindia.csr.database.PusherConnection;
import com.pusher.rest.Pusher;

public class NotificationBuilder {
	
	public static void sendAddPostNotification(String user, String message){
	
		Pusher pusher = PusherConnection.getConnection();
		if(pusher!=null){
		pusher.trigger("Cairn CSR","Post Update",Collections.singletonMap(user,message));
		System.out.println(pusher.toString());
		System.out.println("Connected to pusher service");
		}else{
			System.out.println("Not connected to Pusher Service");
		}
	}
	

}
